var clockJs = {
    i: 0, // only from 1 to 60
    ms: 0, // milisec
    s: 0, // seconds
    ss: 0, // seconds
    m: 0, // minutes
    mm: 0, // minutes
    addNumber: function (i, ii) {


        if (ii < 0) {
            str = '<img src="' + this.images[i] + '" />';
            //console.log(" < 0 " + this.images[i]);
        } else {
            str = '<img src="' + this.images[ii] + '" /><img src="' + this.images[i] + '" />';
            //console.log(this.images[i] + " > 0 " + this.images[ii]);
        }

        return str;
    },
    sentences: [
        ["-Привет", "-Здравствуй", "-Доброго времени суток"],
        ["!!!!", ")))))", "....", "!!!!)))"],
        ["-Который час?", "-Сколько времени?"],
        ["-Слишком поздно", "-Очень рано", "-В самый раз"],
        ["-Время лечит", "-Время дороже денег", "-Береги время"]
    ],
    zero: function () {
        i = 0;
        s = 0;
        m = 0;
        ss = 0;
        mm = 0;
        ms = 0;
    },
    speach: function () {
        $('#clockPhrase').html("");
        $('#clockPhrase2').html("");
        $('#clockPhrase3').html("");
        $('#clockPhrase4').html("");
        writePhrase(clockJs.sentences[0][clockJs.randomText(3)] + clockJs.sentences[1][clockJs.randomText(4)], 'clockPhrase', 5);
        $("#clockPhrase .line").removeClass("green").addClass("red");
        setTimeout(function () {
            writePhrase(clockJs.sentences[2][clockJs.randomText(2)], 'clockPhrase2', 5);
            $("#clockPhrase2 .line").removeClass("red").addClass("green");
            setTimeout(function () {
                writePhrase(clockJs.sentences[3][clockJs.randomText(3)], 'clockPhrase3', 5);
                $("#clockPhrase3 .line").removeClass("green").addClass("red");

                setTimeout(function () {
                    writePhrase(clockJs.sentences[4][clockJs.randomText(3)], 'clockPhrase4', 5)
                    $("#clockPhrase4 .line").removeClass("red").addClass("green");
                }, 2000);

            }, 2000);

        }, 2000);
    }, // Функция обрабатывающие текстовые данные
    images: ["https://static3.depositphotos.com/1007168/261/i/950/depositphotos_2610443-stock-photo-funny-cartoon-numbers-0.jpg", "https://cdn2.vectorstock.com/i/1000x1000/54/76/funny-cartoon-friendly-number-1-one-guy-vector-1425476.jpg", "https://4stor.ru/forum/uploads/attachment/2017-10/1509386086_9czxxqyce.jpeg",
        "https://st2.depositphotos.com/1007168/6108/v/950/depositphotos_61085017-stock-illustration-funny-cartoon-3-number.jpg",
        "https://st2.depositphotos.com/1007168/6106/v/950/depositphotos_61063957-stock-illustration-%D0%B7%D0%B0%D0%B1%D0%B0%D0%B2%D0%BD%D1%8B%D0%B9-%D0%BC%D1%83%D0%BB%D1%8C%D1%82%D1%8F%D1%88%D0%BD%D1%8B%D0%B9-4-%D0%BD%D0%BE%D0%BC%D0%B5%D1%80.jpg",
        "https://ds04.infourok.ru/uploads/ex/1108/0018dcaf-03a4b599/hello_html_2b147a28.jpg",
        "https://cdn2.vectorstock.com/i/1000x1000/16/71/funny-cartoon-numbers-6-vector-801671.jpg", "https://thumbs.dreamstime.com/z/7-%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%BE%D0%B2-%D1%88%D0%B0%D1%80%D0%B6%D0%B0-%D1%81%D0%BC%D0%B5%D1%88%D0%BD%D1%8B%D1%85-13023249.jpg", "https://img2.freepng.ru/20180130/aow/kisspng-cartoon-royalty-free-stock-photography-clip-art-happy-cartoon-number-8-5a702404e423f9.3138318715172986929345.jpg", "https://cdn5.vectorstock.com/i/1000x1000/16/74/funny-cartoon-numbers-9-vector-801674.jpg"], // хранилище данных images
    randomText: function (length) {
        var num = Math.floor((Math.random() * length) + 1);
        return num - 1;
    },
    interval: setInterval(function () {
        ms++;
        if (ms > 9) {
            ms = 0;
        }
        $('#milisec').html(clockJs.addNumber(ms, -1));
    }),
    interval2: setInterval(function () {

        $('#sec').html(clockJs.addNumber(s, ss));
        $('#min').html(clockJs.addNumber(m, mm));

        s++;
        i++;

        if (s % 2) {
            $('.dot').css("color", "white");
        } else {
            $('.dot').css("color", "black");
        }

        // counter

        if (i > 60) {
            i = 0;
            s = 0
            m++;
        }

        // counter s

        if (s > 9) {
            s = 0;
            ++ss;
            clockJs.speach()
        }

        // counter ss

        if (ss >= 6) {
            ss = 0;
        }

        if (mm > 60) {
            mm = 0;
        }


        if (m >= 5) {
            m = 0;
            mm++;
        }
    }, 1000)
};

var i = 0; // only from 1 to 60
var ms = 0; // milisec
var s = 0; // seconds
var ss = 0; // seconds
var m = 0; // minutes
var mm = 0; // minutes
