

//palindrome('racecar') === true;
//palindrome('table') === false;


function palindrome (str){
    var testStr = str.split('').reverse().join('');
    return str === testStr;
}

$(function () {
    $.get('http://numbersapi.com/random/year', function (data) {
        $('#numberYear').text(data);
    });
});

$(function () {
    $.get('http://numbersapi.com/random/date', function (data) {
        $('#numberDate').text(data);
    });
});

(function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 71)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Scroll to top button appear
    $(document).scroll(function () {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 80
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if (typeof $("#mainNav").offset() !== 'undefined') {
            if ($("#mainNav").offset().top > 100) {
                $("#mainNav").addClass("navbar-shrink");
            } else {
                $("#mainNav").removeClass("navbar-shrink");
            }
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

    // Floating label headings for the contact form
    $(function () {
        $("body").on("input propertychange", ".floating-label-form-group", function (e) {
            $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
        }).on("focus", ".floating-label-form-group", function () {
            $(this).addClass("floating-label-form-group-with-focus");
        }).on("blur", ".floating-label-form-group", function () {
            $(this).removeClass("floating-label-form-group-with-focus");
        });
    });

})(jQuery); // End of use strict

function baseBird() {
    this.fly = function (bird, distance, direction) {

        if (bird === 'duck') {
            $("#duck").attr({src: "https://thumbs.gfycat.com/IndolentFearlessGazelle-size_restricted.gif"});

            if (direction === 'back') {
                $("#duck").animate({'marginLeft': distance + 'px'}, 2000);
            } else {
                $("#duck").animate({'marginLeft': distance + 'px'}, 2000);
            }
        }
        if (bird === 'peacock') {
            $("#peacock").attr({src: "https://i.gifer.com/7pO3.gif"});
            if (direction === 'back')
                $("#peacock").animate({'marginLeft': distance + 'px'}, 2000);
            else
                $("#peacock").animate({'marginLeft': distance + 'px'}, 2000)
        }
        if (bird === 'dove') {
            $("#dove").attr({src: "https://i.pinimg.com/originals/d2/14/d7/d214d7925b31b295293a7a7f7b7cfd0d.gif"});
            if (direction === 'back')
                $("#dove").animate({'marginLeft': distance + 'px'}, 2000);
            else
                $('#dove').animate({'marginLeft': distance + 'px'}, 2000)
        }

    };
};
var duck = new baseBird();

duck.swim = function () {
    $("#duck").attr({src: "https://sun9-59.userapi.com/impg/uVkdeXTbBFKh1itgu-gKoNcxZZOhB9uBxvd-HA/s8hg8QQtNBU.jpg?size=450x450&quality=96&proxy=1&sign=38f12c60b5e392798c616e2e3569fc92&type=album"});
}

baseBird.prototype.beak = 1;

var duck2 = new baseBird();

var peacock = new baseBird();

peacock.fly = function () {
    $("#peacock").attr({src: "https://cdn.pixabay.com/photo/2017/09/11/21/27/peacock-2740513_1280.png"});
}

peacock.showTail = function () {
    $("#peacock").attr({src: "https://cdn.pixabay.com/photo/2017/09/11/21/27/peacock-2740513_1280.png"});
}
peacock.hideTail = function () {
    $("#peacock").attr({src: "https://i.pinimg.com/736x/dc/c6/ea/dcc6ea4863bbfb56e66ec5b2593fa02c.jpg"});
}

var dove = new baseBird();
dove.seat = function () {
    $("#dove").attr({src: "https://shutniki.club/wp-content/uploads/2019/12/golub_raskraska_27_28112328.jpg"});
}

var newDuck = function () {
    $("#ducks").append('<img class="duck" src="https://thumbs.gfycat.com/IndolentFearlessGazelle-size_restricted.gif"/>');

}
var newPeacock = function () {
    $("#peacocks").append('<img class="peacock" src="https://i.gifer.com/7pO3.gif"/>');
}
var newDove = function () {
    $("#doves").append('<img class="dove" src="https://i.pinimg.com/originals/d2/14/d7/d214d7925b31b295293a7a7f7b7cfd0d.gif"/>');
}

var duckFly = function () {
    $(".duck").animate({'marginLeft': 200 + 'px'}, 2000);
    setTimeout(function () {
        $(".duck").animate({'marginLeft': '0px'}, 2000);
    }, 4000);
}
var peacockFly = function () {
    $(".peacock").animate({'marginLeft': 200 + 'px'}, 2000);
    setTimeout(function () {
        $(".peacock").animate({'marginLeft': '0px'}, 2000);
    }, 4000);
}
var doveFly = function () {
    $(".dove").animate({'marginLeft': 200 + 'px'}, 2000);
    setTimeout(function () {
        $(".dove").animate({'marginLeft': '0px'}, 2000);
    }, 4000);
}
$(document).ready(function () {
    $('#forecast').click(function () {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "http://api.weatherapi.com/v1/forecast.json?key=7882c89eb2eb485192d135950210802&q=Saint-Petersburg&days=10",
            success: function (data) {
                $("#wheather").html("");
                writePhrase("saint-petersburg", "wheather", 15);
                console.log(data);
                $("#weatherSpb").html(data.location.country + "<br/>" + data.location.region + "<br/>" + data.location.localtime + "<br/> temperature <b>" +
                    data.current.temp_c + "</b><br/><br/> Maximum temperature tomorrow <b> " + data.forecast.forecastday[0].day.maxtemp_c +
                    "</b><br/><br/> Minimum temperature tomorrow <b> " + data.forecast.forecastday[0].day.mintemp_c + "</b>");
                $("#weatherSpb").append("<br/><img src='" + data.current.condition.icon + "'/>");

                $("#wheather .line").css('background-image', 'url(http:' + data.current.condition.icon + ')');
                $("#wheather .line").css('background-size' , '10px 10px');
                $("#wheather .line").removeClass("red").removeClass("green").addClass("blue");
                //$("#wheather").html("");
                //$("#wheather").append($("#ab").html());
            }
        });
    });
    $('#ajax').click(function () {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "http://api.weatherapi.com/v1/current.json?key=7882c89eb2eb485192d135950210802&q=moscow",
            success: function (data) {
                $("#wheather").html("");
                writePhrase("moskow", "wheather", 20);
                console.log(data);
                $("#weatherMsk").html(data.location.country + "<br/>" +
                    data.location.region + "<br/>" + data.location.localtime + "<br/> temperature <b>" +
                    data.current.temp_c + "</b><br/>");
                $("#weatherMsk").append("<img src='" + data.current.condition.icon + "'/>");

                $("#wheather .line").css('background-image', 'url(http:' + data.current.condition.icon + ')');
                $("#wheather .line").css('background-size' , '10px 10px');
                $("#wheather .line").removeClass("red").removeClass("green").addClass("blue");

                //$("#wheather").append($("#ab").html());
                //$("#ab").html("");
            }
        });
    });
    $('#weatherDenpasar').click(function () {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "http://api.weatherapi.com/v1/current.json?key=7882c89eb2eb485192d135950210802&q=Denpasar&",
            success: function (data) {
                $("#wheather").html("");
                writePhrase("denpasar", "wheather", 20);
                console.log(data);
                $("#weatherDenpasarDiv").html(data.location.country + "<br/>" +
                    data.location.region + "<br/>" + data.location.localtime + "<br/> temperature <b>" +
                    data.current.temp_c + "</b><br/>");
                $("#weatherDenpasarDiv").append("<img src='" + data.current.condition.icon + "'/>");

                $("#wheather .line").css('background-image', 'url(http:' + data.current.condition.icon + ')');
                $("#wheather .line").css('background-size' , '10px 10px');
                $("#wheather .line").removeClass("red").removeClass("green").addClass("blue");
            }

        });
    });
});

