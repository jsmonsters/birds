// git add .
// git commit -m lalala
// git push
// git pull
// git clone
// hahaha


// You need ask questions
// 1. Your age?
// 2. Your Name name?
// 3. Color of your eyes?
// 4. What your preferable season?


//----------------
// If User come to us first time, you need ask 4 questions.
// If we know who is it, just show answers.

let isMomHappy = true;

try {
    isMomHappy++;
} catch (error) {
    console.log(error)
    isMomHappy = false;
}


// Promise
var willIGetNewPhone = new Promise(
    function (resolve, reject) {
        if (isMomHappy) {
            var phone = {
                brand: 'Samsung',
                color: 'black'
            };
            console.log(phone);
            resolve(phone); // fulfilled
        } else {
            var reason = new Error('mom is not happy');
            console.log(reason);
            reject(reason); // reject
        }
    }
);

// call our promise
var askMom = function () {
    willIGetNewPhone
        .then(function (fulfilled) {
            // yay, you got a new phone
            console.log(fulfilled);
            // output: { brand: 'Samsung', color: 'black' }
        })
        .catch(function (error) {
            // oops, mom don't buy it
            console.log(error.message);
            // output: 'mom is not happy'
        });
};

askMom();

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function deleteCookie(name) {
    createCookie(name, "", -1);
}

createCookie('name', 'Sofia', 20)
createCookie('Sofia', 'I am', 31)

function yourName() {
    var person = prompt("Please enter your name", "Type here");
    deleteCookie("visitor");
    createCookie("visitor", person, 5)
    if (person != null) {

        document.getElementById("demo").innerHTML =
            "Hello " + person + "! How are you today?";
    }
}

//yourName();

function questions() {
    if (readCookie('guest')) {
        var fromCookies = readCookie('guest');
        var fromCookiesArr = fromCookies.split(',');
        var str = "Hi!!<br/>" +
            " Your age is " + fromCookiesArr[0] + "<br/>" +
            " Your name " + fromCookiesArr[1] + "<br/>" +
            " color of Your eyes is " + fromCookiesArr[2] + "<br/>" +
            " Your preferable season is " + fromCookiesArr[3] + "!";
        $("#answer").html(str)
    } else {
        var question = [prompt('Your age?'),
            prompt('Your name?'),
            prompt('Color of your eyes?'),
            prompt('What your preferable season?')]
        createCookie("guest", question, 30)
    }
}

setTimeout(function () {
    questions();
}, 5000);


function getJson() {
    console.log('Who is here?');
    console.log(readCookie('name'));

    $.ajax({
        url: "example.json", //url
        cache: false
    })
        .done(function (data) {
            console.log(data[0].symbol); //obj
            $("#text").text(data[0].symbol);
            setTimeout(() => {
                $("#text").text(data[0].symbol + ' hahahaha');
            }, 3000);
        });
}

function baseBird() {

    this.legs = 2;
    this.head = 1;
    this.tail = 1;
    this.wings = 2;
    this.fly = function (bird, distance, direction) {

        if (bird === 'duck') {
            $("#duck").attr({src: "https://thumbs.gfycat.com/IndolentFearlessGazelle-size_restricted.gif"});

            if (direction === 'back') {
                $("#duck").animate({'marginLeft': distance + 'px'}, 2000);
            } else {
                $("#duck").animate({'marginLeft': distance + 'px'}, 2000);
            }
        }
        if (bird === 'peacock') {
            $("#peacock").attr({src: "https://i.gifer.com/7pO3.gif"});
            if (direction === 'back')
                $("#peacock").animate({'marginLeft': distance + 'px'}, 2000);
            else
                $("#peacock").animate({'marginLeft': distance + 'px'}, 2000)
        }
        if (bird === 'dove') {
            $("#dove").attr({src: "https://i.pinimg.com/originals/d2/14/d7/d214d7925b31b295293a7a7f7b7cfd0d.gif"});
            if (direction === 'back')
                $("#dove").animate({'marginLeft': distance + 'px'}, 2000);
            else
                $('#dove').animate({'marginLeft': distance + 'px'}, 2000)
        }

    };
};

function whatInside(obj) {
    var str = '';
    for (i in obj) {
        str += i + ' <a target="_blank" href="https://www.google.com/search?q=js+' + i + '">' + i + '</a><br /> '
    }
    document.write(str);
}

var duck = new baseBird();


console.log('duck.beak');
console.log(duck.beak);

duck.swim = function () {
    $("#duck").attr({src: "https://sun9-59.userapi.com/impg/uVkdeXTbBFKh1itgu-gKoNcxZZOhB9uBxvd-HA/s8hg8QQtNBU.jpg?size=450x450&quality=96&proxy=1&sign=38f12c60b5e392798c616e2e3569fc92&type=album"});
}

baseBird.prototype.beak = 1;
console.log(baseBird.name);


console.log('duck.beak');
console.log(duck.beak);

var duck2 = new baseBird();

console.log('duck2.beak');
console.log(duck2.beak);


var peacock = new baseBird();


console.log('peacock.beak');
console.log(peacock.beak);


console.log('peacock.legs');
console.log(peacock.legs);

peacock.fly = function () {
    $("#peacock").attr({src: "https://cdn.pixabay.com/photo/2017/09/11/21/27/peacock-2740513_1280.png"});
}

peacock.showTail = function () {
    $("#peacock").attr({src: "https://cdn.pixabay.com/photo/2017/09/11/21/27/peacock-2740513_1280.png"});
}
peacock.hideTail = function () {
    $("#peacock").attr({src: "https://i.pinimg.com/736x/dc/c6/ea/dcc6ea4863bbfb56e66ec5b2593fa02c.jpg"});
}

var dove = new baseBird();
dove.seat = function () {
    $("#dove").attr({src: "https://shutniki.club/wp-content/uploads/2019/12/golub_raskraska_27_28112328.jpg"});
}

var newDuck = function () {
    $("#ducks").append('<img class="duck" src="https://thumbs.gfycat.com/IndolentFearlessGazelle-size_restricted.gif"/>');

}
var newPeacock = function () {
    $("#peacocks").append('<img class="peacock" src="https://i.gifer.com/7pO3.gif"/>');
}
var newDove = function () {
    $("#doves").append('<img class="dove" src="https://i.pinimg.com/originals/d2/14/d7/d214d7925b31b295293a7a7f7b7cfd0d.gif"/>');
}

var duckFly = function () {
    $(".duck").animate({'marginLeft': 200 + 'px'}, 2000);
    setTimeout(function () {
        $(".duck").animate({'marginLeft': '0px'}, 2000);
    }, 4000);
}
var peacockFly = function () {
    $(".peacock").animate({'marginLeft': 200 + 'px'}, 2000);
    setTimeout(function () {
        $(".peacock").animate({'marginLeft': '0px'}, 2000);
    }, 4000);
}
var doveFly = function () {
    $(".dove").animate({'marginLeft': 200 + 'px'}, 2000);
    setTimeout(function () {
        $(".dove").animate({'marginLeft': '0px'}, 2000);
    }, 4000);
}